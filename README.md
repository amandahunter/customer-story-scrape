# My project's README

The scraper uses Python’s BeautifulSoup toolkit to parse the site’s HTML, extract the data and export to a CSV. This code uses the customer stories on the Salesforce UK website.
