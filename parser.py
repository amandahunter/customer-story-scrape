from bs4 import BeautifulSoup

class Parser:
    def __init__(self,data, parser_obj):
        self.data=data
        self.parser_obj = parser_obj #instance of customer object
        
    def parse_pages(self):
        
        soup = BeautifulSoup(self.data, "html.parser")
        
        
        self.parser_obj.customerName = soup.find_all("h1")
        #self.parser_obj.blockquote = soup.find_all("blockquote", class_="customer-quote")
        self.parser_obj.title = soup.find_all("h3")
        bq = soup.find_all("blockquote", class_="customer-quote")
        self.parser_obj.blockquote  =  bq[0].get_text()
        '''for node in self.parser_obj.blockquote:
            print node
           node.findAll(text=True)
            return self.parser_obj.blockText'''
        return self.parser_obj
    

       
